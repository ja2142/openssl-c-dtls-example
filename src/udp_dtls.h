#include <span>
#include <thread>

#include <boost/asio.hpp>

#include <openssl/bio.h>
#include <openssl/ssl.h>

// unique_ptr c wrapper, that knows how to free underlying resource
template <typename T>
using unique_ptr_c = std::unique_ptr<T, std::function<void(T*)>>;

class UdpDtls;

using handle_connection_callback_t = void(std::unique_ptr<UdpDtls> connection);

class UdpDtls {
   public:
    static const int COOKIE_KEY_EX_DATA_IDX = 0;
    static const int COOKIE_KEY_LENGTH = 16;

    UdpDtls(boost::asio::io_service& io_service);
    UdpDtls(boost::asio::ip::udp::socket socket,
            unique_ptr_c<SSL> existing_connection,
            BIO* io);
    void listen(boost::asio::ip::udp::endpoint bind_address,
                handle_connection_callback_t handle_connection_callback);
    bool connect(boost::asio::ip::udp::endpoint server_endpoint);
    void send(std::span<char> data);
    std::string receive();

    inline boost::asio::ip::udp::socket& get_socket() { return socket; }
    inline BIO_ADDR* get_remote_address() { return remote_address.get(); }

   private:
    void handle_client_connection(
        boost::asio::ip::udp::socket& socket,
        BIO_ADDR* client_address,
        handle_connection_callback_t handle_connection_callback);
    void setup_common(const SSL_METHOD* ssl_method,
                      std::string cert_filename,
                      std::string key_filename);
    boost::asio::ip::udp::socket socket;
    unique_ptr_c<BIO_ADDR> remote_address;
    unique_ptr_c<SSL_CTX> ssl_context;
    unique_ptr_c<SSL> ssl;
    BIO* bio;
    char cookie_key[COOKIE_KEY_LENGTH];

    std::vector<std::jthread> client_handlers;
};
