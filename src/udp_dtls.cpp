#include "udp_dtls.h"

#include <iostream>

#include <openssl/err.h>
#include <openssl/rand.h>
#include <openssl/x509_vfy.h>

using namespace boost::asio;
using ip::udp;

namespace {

const int IPV4_SIZE = 4;
const int IPV6_SIZE = 16;

void SSL_close_and_free(SSL* ssl) {
    SSL_shutdown(ssl);
    SSL_free(ssl);
}

void print_errors_stderr() {
    ERR_print_errors_cb(
        [](const char* str, size_t len, void* u) {
            std::cerr << str;
            return 0;
        },
        nullptr);
}

udp::endpoint BIO_ADDR_to_endpoint(const BIO_ADDR* address) {
    // all of these are in network order (as retrurned by BIO_ADDR_raw
    // functions)
    uint32_t ip_address_v4_network;
    ip::address_v6::bytes_type ip_address_v6_network;
    uint16_t port_network;

    switch (BIO_ADDR_family(address)) {
        case AF_INET:
            BIO_ADDR_rawaddress(address, &ip_address_v4_network, NULL);
            port_network = BIO_ADDR_rawport(address);
            return udp::endpoint(ip::address_v4(ntohl(ip_address_v4_network)),
                                 ntohs(port_network));

        case AF_INET6:
            BIO_ADDR_rawaddress(address, &ip_address_v6_network, NULL);
            port_network = BIO_ADDR_rawport(address);
            return udp::endpoint(ip::address_v6(ip_address_v6_network),
                                 ntohs(port_network));
        default:
            throw std::logic_error(
                "only AF_INET and AF_INET6 can be converted to boost "
                "udp::endpoint");
            break;
    }
}

void endpoint_to_BIO_ADDR(unique_ptr_c<BIO_ADDR>& address,
                          udp::endpoint endpoint) {
    if (!address) {
        address = unique_ptr_c<BIO_ADDR>(BIO_ADDR_new(), BIO_ADDR_free);
    }
    if (endpoint.address().is_v4()) {
        uint32_t addr = htonl(endpoint.address().to_v4().to_uint());
        BIO_ADDR_rawmake(address.get(), AF_INET, &addr, IPV4_SIZE,
                         endpoint.port());
    } else if (endpoint.address().is_v6()) {
        in6_addr addr;
        inet_pton(AF_INET6, endpoint.address().to_v6().to_string().c_str(),
                  &addr);
        BIO_ADDR_rawmake(address.get(), AF_INET6, &addr, IPV6_SIZE,
                         endpoint.port());
    }
}

int verify_certificate(int ok, X509_STORE_CTX* ctx) {
    // only trust certificates that were verified
    return ok;
}

int generate_cookie(SSL* ssl, unsigned char* cookie, unsigned int* cookie_len) {
    unique_ptr_c<BIO_ADDR> remote_address =
        unique_ptr_c<BIO_ADDR>(BIO_ADDR_new(), BIO_ADDR_free);
    BIO_dgram_get_peer(SSL_get_rbio(ssl), remote_address.get());

    std::ostringstream unhashed_cookie_stream;
    unhashed_cookie_stream << BIO_ADDR_to_endpoint(remote_address.get());
    std::string unhashed_cookie = unhashed_cookie_stream.str();

    void* cookie_key = SSL_get_ex_data(ssl, UdpDtls::COOKIE_KEY_EX_DATA_IDX);
    if (!HMAC(EVP_sha256(), cookie_key, UdpDtls::COOKIE_KEY_LENGTH,
              (const unsigned char*)unhashed_cookie.c_str(),
              unhashed_cookie.size(), cookie, cookie_len)) {
        return 0;
    }
    return 1;
}

int verify_cookie(SSL* ssl,
                  const unsigned char* cookie,
                  unsigned int cookie_len) {
    unsigned char generated_cookie[EVP_MAX_MD_SIZE];
    unsigned int generated_cookie_length;
    if (!generate_cookie(ssl, generated_cookie, &generated_cookie_length)) {
        return 0;
    }
    if (generated_cookie_length != cookie_len) {
        return 0;
    }
    if (std::memcmp(cookie, generated_cookie, cookie_len) != 0) {
        return 0;
    }
    return 1;
}

}  // namespace

UdpDtls::UdpDtls(boost::asio::io_service& io_service) : socket(io_service) {}

UdpDtls::UdpDtls(udp::socket socket,
                 unique_ptr_c<SSL> existing_connection,
                 BIO* io = nullptr)
    : socket(std::move(socket)) {
    ssl = std::move(existing_connection);
    bio = io;
}

void UdpDtls::setup_common(const SSL_METHOD* ssl_method,
                           std::string cert_filename,
                           std::string key_filename) {
    int status = 0;

    // these two should only be called once for entire application, so this
    // might not be the best place for them
    status = SSL_load_error_strings();
    status = SSL_library_init();

    ssl_context = unique_ptr_c<SSL_CTX>(SSL_CTX_new(ssl_method), SSL_CTX_free);

    auto cert_store =
        unique_ptr_c<X509_STORE>(X509_STORE_new(), X509_STORE_free);

    X509_STORE_load_locations(cert_store.get(), "pki/ca_cert.pem", nullptr);
    status =
        SSL_CTX_set1_verify_cert_store(ssl_context.get(), cert_store.get());

    if (status = SSL_CTX_use_certificate_chain_file(ssl_context.get(),
                                                    cert_filename.c_str())) {
        print_errors_stderr();
    }
    if (status = SSL_CTX_use_PrivateKey_file(
            ssl_context.get(), key_filename.c_str(), SSL_FILETYPE_PEM)) {
        print_errors_stderr();
    }

    SSL_CTX_set_verify(ssl_context.get(), SSL_VERIFY_PEER, verify_certificate);
    SSL_CTX_set_cookie_generate_cb(ssl_context.get(), generate_cookie);
    SSL_CTX_set_cookie_verify_cb(ssl_context.get(), verify_cookie);

    remote_address = unique_ptr_c<BIO_ADDR>(BIO_ADDR_new(), BIO_ADDR_free);
}

void UdpDtls::listen(udp::endpoint bind_address,
                     handle_connection_callback_t handle_connection_callback) {
    setup_common(DTLS_server_method(), "pki/server_cert.pem",
                 "pki/server_key.pem");

    RAND_bytes((unsigned char*)cookie_key, COOKIE_KEY_LENGTH);

    socket.open(bind_address.protocol());
    boost::asio::socket_base::reuse_address reuse(true);
    socket.set_option(reuse);
    socket.bind(bind_address);

    std::cout << "listening on " << bind_address << "\n";

    while (1) {
        int status = 0;
        bio = BIO_new_dgram(socket.native_handle(), BIO_NOCLOSE);

        ssl = unique_ptr_c<SSL>(SSL_new(ssl_context.get()), SSL_close_and_free);
        SSL_set_bio(ssl.get(), bio, bio);
        SSL_set_ex_data(ssl.get(), COOKIE_KEY_EX_DATA_IDX, cookie_key);

        SSL_set_options(ssl.get(), SSL_OP_COOKIE_EXCHANGE);

        while (status <= 0) {
            status = DTLSv1_listen(ssl.get(), remote_address.get());
            if (status <= 0) {
                print_errors_stderr();
            }
        }

        std::cout << "new connection from "
                  << BIO_ADDR_to_endpoint(remote_address.get()) << "\n";

        handle_client_connection(socket, remote_address.get(),
                                 handle_connection_callback);
    }
}

bool UdpDtls::connect(udp::endpoint server_endpoint) {
    int status = 0;
    setup_common(DTLS_client_method(), "pki/client_cert.pem",
                 "pki/client_key.pem");

    std::cout << "connecting to " << server_endpoint << "\n";

    socket.connect(server_endpoint);

    bio = BIO_new_dgram(socket.native_handle(), BIO_NOCLOSE);
    endpoint_to_BIO_ADDR(remote_address, server_endpoint);

    status =
        BIO_ctrl(bio, BIO_CTRL_DGRAM_SET_CONNECTED, 0, remote_address.get());
    ssl = unique_ptr_c<SSL>(SSL_new(ssl_context.get()), SSL_close_and_free);
    SSL_set_bio(ssl.get(), bio, bio);
    status = SSL_connect(ssl.get());
    if (status <= 0) {
        print_errors_stderr();
        return false;
    } else {
        std::cout << "connnected\n";
        return true;
    }
}

void UdpDtls::handle_client_connection(
    udp::socket& server_socket,
    BIO_ADDR* client_address,
    handle_connection_callback_t handle_connection_callback) {
    int status = 0;
    udp::socket client_socket(server_socket.get_executor());

    client_socket.open(server_socket.local_endpoint().protocol());
    boost::asio::socket_base::reuse_address reuse(true);
    client_socket.set_option(reuse);
    client_socket.bind(server_socket.local_endpoint());
    client_socket.connect(BIO_ADDR_to_endpoint(client_address));

    BIO* client_bio = SSL_get_rbio(ssl.get());
    BIO_set_fd(client_bio, client_socket.native_handle(), BIO_NOCLOSE);
    BIO_ctrl(client_bio, BIO_CTRL_DGRAM_SET_CONNECTED, 0, &client_address);

    if (SSL_accept(ssl.get()) != 1) {
        print_errors_stderr();
    }

    client_handlers.emplace_back(
        [handle_connection_callback](std::unique_ptr<UdpDtls> connection) {
            try {
                handle_connection_callback(std::move(connection));
            } catch (std::exception& e) {
                std::cerr << "exception when handling client: " << e.what()
                          << "\n";
            }
        },
        std::make_unique<UdpDtls>(std::move(client_socket), std::move(ssl),
                                  bio));
}

void UdpDtls::send(std::span<char> data) {
    int n = SSL_write(ssl.get(), data.data(), data.size());
    if (n <= 0) {
        print_errors_stderr();
        throw std::runtime_error("SSL_write failed");
    }
}

std::string UdpDtls::receive() {
    const int MAX_READ_SIZE = 256;
    char b[MAX_READ_SIZE];
    int n = SSL_read(ssl.get(), b, MAX_READ_SIZE);
    if (n <= 0) {
        print_errors_stderr();
        throw std::runtime_error("SSL_read failed");
    }
    return std::string(b, n);
}
