#include <functional>
#include <iostream>
#include <random>

#include <boost/program_options.hpp>

#include "udp_dtls.h"

using namespace boost;
using namespace asio;
using ip::udp;

const int PORT = 11034;

auto init_random_gen() {
    std::default_random_engine generator;
    generator.seed(std::chrono::system_clock::now().time_since_epoch().count());
    std::uniform_int_distribution<int> distribution(0, 0x10000);
    return std::bind(distribution, generator);
}

int main(int argc, char** argv) {
    using program_options::value;

    program_options::options_description desc{"options"};
    desc.add_options()("help,h", "help")(
        "connect,c", value<std::string>()->default_value("127.0.0.1"),
        "address to connect to")("port,p", value<short>()->default_value(PORT),
                                 "port to connect to");

    program_options::variables_map options;
    store(parse_command_line(argc, argv, desc), options);

    if (options.count("help")) {
        std::cout << desc << '\n';
        return 0;
    }

    io_service service;
    UdpDtls dtls(service);

    auto get_random = init_random_gen();

    if (!dtls.connect(udp::endpoint(
            ip::address::from_string(options["connect"].as<std::string>()),
            options["port"].as<short>()))) {
        std::cerr << "connection failed\n";
        return 1;
    }

    for (int i = 0; i < 16; i++) {
        std::string to_send =
            std::to_string(get_random()) + "+" + std::to_string(get_random());
        std::cout << "sending to " << dtls.get_socket().remote_endpoint()
                  << ": " << to_send << "\n";
        dtls.send(to_send);

        std::string received = dtls.receive();
        std::cout << "received from " << dtls.get_socket().remote_endpoint()
                  << ": " << received << "\n";
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
}
