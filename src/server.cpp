#include <iostream>

#include <boost/program_options.hpp>

#include "udp_dtls.h"

using namespace boost;
using namespace asio;
using ip::udp;

const int PORT = 11034;

int calculate(std::string calculation) {
    // assuming calculation is "int+int"
    auto plus_pos = calculation.find("+");
    int operand0 = std::stoi(calculation.substr(0, plus_pos));
    int operand1 = std::stoi(calculation.substr(plus_pos + 1));
    return operand0 + operand1;
}

void handle_client(std::unique_ptr<UdpDtls> connection) {
    while (true) {
        std::string received = connection->receive();
        std::cout << "received from "
                  << connection->get_socket().remote_endpoint() << ": "
                  << received << "\n";
        int result = calculate(received);
        received += "=";
        received += std::to_string(result);
        std::cout << "sending to " << connection->get_socket().remote_endpoint()
                  << ": " << received << "\n";
        connection->send(received);
    }
}

int main(int argc, char** argv) {
    using program_options::value;

    program_options::options_description desc{"options"};
    desc.add_options()("help,h", "help")(
        "listen,l", value<std::string>()->default_value("127.0.0.1"),
        "address to listen on")("port,p", value<short>()->default_value(PORT),
                                "port to listen on");

    program_options::variables_map options;
    store(parse_command_line(argc, argv, desc), options);

    if (options.count("help")) {
        std::cout << desc << '\n';
        return 0;
    }
    io_service service;
    UdpDtls dtls(service);

    dtls.listen(udp::endpoint(ip::address::from_string(
                                  options["listen"].as<std::string>()),
                              options["port"].as<short>()),
                handle_client);
}
