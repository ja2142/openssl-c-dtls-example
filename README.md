# OpenSSL C++ DTLS example

An example of openssl DTLS server and client, in C++. With cookie generation, custom CA and multiple clients.

Error handling is not great, and nothing's very flexible, but at least it does show that using openssl from C++ is pretty annoying.

## Dependencies

Requires openssl (obviously), boost and cmake for building. Python ansicolors are optional for launch script.

Probably installable with something like:
```
apt install cmake libboost-all-dev openssl-dev
```
```
pacman -S cmake boost boost-libs openssl
```
or similar.

For colors:
```
pip3 install ansicolors
```

## Build and run

### Build:

```
mkdir build && cd build
cmake ..
cmake --build .
```

### Generate certs

```
./gen_pki.py
```
This generates a CA, and server and client certs signed with it.

### Run

```
./build/server
./build/client
```

Cert and key paths are hardcoded, binaries have to be launched like that.

### Or do all of the above:

```
./build_and_run.py
```

This builds executables, generates certs, and launches server and 3 clients.
