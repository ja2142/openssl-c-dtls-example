#!/usr/bin/env python3

from threading import Thread
from subprocess import Popen, PIPE, STDOUT
import subprocess
from pathlib import Path
from time import sleep
import pty
import os

from gen_pki import gen_pki

try:
    from colors import color
except ModuleNotFoundError():
    print('no color support, "pip install ansicolors" for nice colors')
    def color(s, args, kwargs):
        # fallback when there's no colors
        return s

def handle_outputs(pipes):
    def handle_output(pipe, name, name_color):
        while True:
            try:
                line = pipe.readline().strip()
            except OSError:
                break
            if line == '':
                break
            print(color(name, name_color) + ': ' + line)

    threads = [Thread(target = handle_output, name=name, args=(pipe, name, color), daemon=True)
        for pipe, name, color in pipes]
    for thread in threads:
        thread.start()
    return threads

CLIENT_COLORS = ['yellow', 'orange', 'green']
SERVER_COLOR = 'blue'

print('building project')
Path('build').mkdir(exist_ok=True)
subprocess.run(['cmake', '..'], cwd='build')
subprocess.run(['cmake', '--build', '.'], cwd='build')

print('generating pki')
gen_pki()

print('starting server')
server_rp, server_wp = (os.fdopen(pipe) for pipe in pty.openpty())
server = Popen(['./build/server'], stdout = server_wp, stderr = STDOUT, encoding='utf-8', bufsize=1)

print('starting clients')
clients = []
clients_pipes = []
for i in range(3):
    client_rp, client_wp = (os.fdopen(pipe) for pipe in pty.openpty())
    clients_pipes.append(client_rp)
    clients.append(Popen(['./build/client'], stdout = client_wp, stderr = STDOUT, encoding='utf-8', bufsize=1))

clients_pipes = list(zip(clients_pipes, [f'client {i}' for i in range(3)], CLIENT_COLORS))

all_pipes = clients_pipes + [(server_rp, 'server', SERVER_COLOR)]
threads = handle_outputs(all_pipes)

while clients:
    clients = list(filter(lambda t: t.poll() == None, clients))
    sleep(0.5)

if server.poll() == None:
    server.kill()
