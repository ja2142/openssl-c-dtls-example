#!/usr/bin/env python3

import subprocess
from pathlib import Path 

PKI_DIR = 'pki'
REQ_DIR = PKI_DIR + '/req'

def run_cmd(cmd):
    return subprocess.run(cmd, shell=True)

def generate_ca():
    print('generating ca key')
    run_cmd(f'openssl genrsa -out {PKI_DIR}/ca_key.pem')

    print('generating ca cert')
    run_cmd(f'openssl req -new -key {PKI_DIR}/ca_key.pem -x509 -out {PKI_DIR}/ca_cert.pem -subj "/C=SC/ST=Victoria/L=Victoria/O=testorg/OU=test/CN=ca"')

def generate_and_sign_cert(name):
    print(f'generating csr for {name}')
    run_cmd(f'openssl req -new -nodes -newkey rsa:4096 -keyout {PKI_DIR}/{name}_key.pem -out {REQ_DIR}/{name}_csr.pem -batch -subj "/C=SC/ST=Victoria/L=Victoria/O=testorg/OU=test/CN={name}"')
    print(f'signing cert for {name}')
    run_cmd(f'openssl x509 -req -in {REQ_DIR}/{name}_csr.pem -CA {PKI_DIR}/ca_cert.pem -CAkey {PKI_DIR}/ca_key.pem -CAcreateserial -out {PKI_DIR}/{name}_cert.pem')

def gen_pki():
    print('creating pki dirs')
    Path(REQ_DIR).mkdir(parents=True, exist_ok=True)

    print('generating ca')
    generate_ca()

    print('generating server cert')
    generate_and_sign_cert('server')

    print('generating client cert')
    generate_and_sign_cert('client')

if __name__ == '__main__':
    gen_pki()
